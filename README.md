Declarative Schema - таблицы<br>
ТЗ<br>
1. Создать db_schema xml<br>
2. Сделать таблицу page с полями id (int, PK), name (varchar 255), created_at (datetime)<br>
3. Создать таблицу page_block с полями id (int, PK), page_id (int, FK > page.id), name (varchar 255), created_at (datetime)<br>
4. Сгенерить db_schema json
